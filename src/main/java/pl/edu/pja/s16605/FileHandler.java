package pl.edu.pja.s16605;


import pl.edu.pja.s16605.exceptions.CouldNotDeleteFileException;
import pl.edu.pja.s16605.exceptions.ElementLengthExceedException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.util.Random;

import static pl.edu.pja.s16605.utils.Log.log;

public class FileHandler {
    static final int kB = 1024;
    static final int MB = kB * 1024;
    File path;
    int fileSize;

    Random random;

    RandomAccessFile file;
    MappedByteBuffer io;

    /**
     * Constructor for noisy file handler
     *
     * @param path     path to the file
     * @param fileSize size of desired file
     * @throws IOException
     */
    public FileHandler(String path, int fileSize) throws Exception {
        random = new Random();
        this.path = new File(path);
        this.fileSize = fileSize;
        if (!this.path.exists()) {
            log("File does not exist. Generating new file.");
            generateFile();
        } else if (this.path.length() != fileSize * MB) {
            log("File size does not match. Generating new...");
            if (this.path.delete())
                generateFile();
            else {
                throw new CouldNotDeleteFileException();
            }
            log("File exists and length matches");
        }
    }

    /**
     * Generates new noisy file for given parameters
     *
     * @throws IOException
     */
    void generateFile() throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        byte[] buffer = new byte[MB];
        long t0 = System.currentTimeMillis();
        for (long i = 0; i < fileSize; i++) {
            random.nextBytes(buffer);
            fos.write(buffer);
        }
        log("New noisy file generated. took " + (System.currentTimeMillis() - t0) + "ms");
    }

    /**
     * Replaces noisy file with a new one
     *
     * @throws Exception
     */
    public void regenerateFile() throws Exception {
        if (path.delete())
            generateFile();
        else throw new CouldNotDeleteFileException();
    }

    /**
     * Puts bytes into file on given position
     *
     * @param element  bytes to be stored
     * @param position position of beginning of the element
     * @throws IOException
     */
    long putElement(byte[] element, long position) throws Exception {
        file = new RandomAccessFile(path, "rw");
        if (fileSize * MB < position + element.length)
            throw new ElementLengthExceedException();
        file.seek(position);
        file.write(element);
        file.close();
        log("Element is put successfully at position " + position);
        return position;
    }

    /**
     * Reads bytes from file
     *
     * @param position starting position
     * @param length   length of desired element
     * @return acquired element
     * @throws IOException
     */
    public byte[] getElement(long position, int length) throws Exception {
        if (fileSize * MB < position + length)
            throw new ElementLengthExceedException();
        byte[] element = new byte[length];
        file = new RandomAccessFile(path, "r");
        file.seek(position);
        file.read(element);
        file.close();
        return element;
    }

    /**
     * Inserts element in random place
     *
     * @param element to be stored
     * @return position of begging of the element
     * @throws Exception
     */
    public int putElementInRandomPlace(byte[] element) throws Exception {
        regenerateFile();
        int position = random.nextInt(fileSize * MB - element.length);
        putElement(element, position);
        return position;
    }

}
