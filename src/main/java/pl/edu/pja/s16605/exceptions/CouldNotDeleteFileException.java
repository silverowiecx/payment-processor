package pl.edu.pja.s16605.exceptions;

public class CouldNotDeleteFileException extends MainException {
    public CouldNotDeleteFileException() {
        super("Could not delete file!");
    }
}
