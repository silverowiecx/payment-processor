package pl.edu.pja.s16605.exceptions;

public class InvalidMessageTypeException extends MainException {
    public InvalidMessageTypeException(byte msgType) {
        super("MessageType (" + msgType + ") does not match Server role so it could not be sent");
    }
}
