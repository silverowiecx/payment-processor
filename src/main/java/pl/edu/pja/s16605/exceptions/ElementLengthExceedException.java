package pl.edu.pja.s16605.exceptions;

public class ElementLengthExceedException extends MainException {
    public ElementLengthExceedException() {
        super("Element length exceeds file size!");
    }
}
