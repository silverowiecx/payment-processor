package pl.edu.pja.s16605.exceptions;

public class IllegalClientStateException extends MainException {
    public IllegalClientStateException() {
        super("An error occurred while message processing");
    }
}
