package pl.edu.pja.s16605.exceptions.BtcExceptions;

import pl.edu.pja.s16605.exceptions.MainException;

public class NotEnoughFeeException extends MainException {
    public NotEnoughFeeException() {
        super("Fee is not sufficient to complete transaction!");
    }
}
