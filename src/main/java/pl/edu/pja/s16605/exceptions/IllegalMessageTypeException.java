package pl.edu.pja.s16605.exceptions;

public class IllegalMessageTypeException extends MainException {
    public IllegalMessageTypeException(byte msgType) {
        super("MessageType (" + msgType + ") does not exist!");
    }
}
