package pl.edu.pja.s16605.exceptions;

public class InvalidResponseException extends MainException {
    public InvalidResponseException() {
        super("This response is invalid or illegal for now!");
    }
}
