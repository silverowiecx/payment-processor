package pl.edu.pja.s16605.exceptions.BtcExceptions;

import pl.edu.pja.s16605.exceptions.MainException;

public class ElectrumException extends MainException {
    public ElectrumException() {
        super("There was a problem getting a response, see above for exit code");
    }
}
