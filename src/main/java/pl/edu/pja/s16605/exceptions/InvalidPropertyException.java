package pl.edu.pja.s16605.exceptions;


public class InvalidPropertyException extends MainException {
    public InvalidPropertyException() {
        super("Could not retrieve property or it is empty!");
    }
}
