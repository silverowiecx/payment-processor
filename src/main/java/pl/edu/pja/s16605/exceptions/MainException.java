package pl.edu.pja.s16605.exceptions;

import java.util.Date;

public class MainException extends Exception {
    public MainException(String msg) {
        super(new Date() + ": " + msg);
    }
}
