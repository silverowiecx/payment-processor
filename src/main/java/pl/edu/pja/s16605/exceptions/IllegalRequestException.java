package pl.edu.pja.s16605.exceptions;

public class IllegalRequestException extends MainException {
    public IllegalRequestException(String msg) {
        super("During privKey decryption: " + msg);
    }
}
