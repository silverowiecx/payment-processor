package pl.edu.pja.s16605;

import pl.edu.pja.s16605.FileHandler;
import pl.edu.pja.s16605.Properties;
import pl.edu.pja.s16605.algorithms.AES;
import pl.edu.pja.s16605.algorithms.RSA;
import pl.edu.pja.s16605.algorithms.Sha256;
import pl.edu.pja.s16605.communication.messages.MessageType;
import pl.edu.pja.s16605.utils.ByteUtils;

import java.security.KeyPair;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Scanner;

public class ProcessorInit {

    public static String fName = String.valueOf(MessageType.BtcRequest) + "K";
    public static String infoKeyEncrypted = String.valueOf(MessageType.BtcRequest) + "IK";
    public static String halfKey32Processor = String.valueOf(MessageType.BtcRequest) + "HK";


    public static void main(String[] args) throws Exception {
        //RSA
        KeyPair kp = RSA.generateKeyPair();
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "PRR", kp.getPrivate().getEncoded());

        System.out.println("First key to be entered on exchange side:");
        System.out.println(ByteUtils.bytesToHex(kp.getPublic().getEncoded()));

        kp = RSA.generateKeyPair();

        System.out.println("Second key to be entered on exchange side:");
        System.out.println(ByteUtils.bytesToHex(kp.getPrivate().getEncoded()));
        Properties.setProperty(String.valueOf(MessageType.BtcResponse) + "PRP", kp.getPublic().getEncoded());


        SecureRandom rand = new SecureRandom();
        byte[] seed = new byte[32];

        rand.nextBytes(seed);
        byte[] exchangePartKey = Sha256.hash(seed);
        System.out.println("Third key to be entered on exchange side:");
        System.out.println(ByteUtils.bytesToHex(exchangePartKey));

        rand.nextBytes(seed);
        byte[] processorPartKey = Sha256.hash(seed);
        Properties.setProperty(halfKey32Processor, processorPartKey);

        byte[] AESforInfoKeyEncyption = new byte[64];
        System.arraycopy(exchangePartKey, 0, AESforInfoKeyEncyption, 0, 32);
        System.arraycopy(processorPartKey, 0, AESforInfoKeyEncyption, 32, 32);

        rand.nextBytes(seed);
        AESforInfoKeyEncyption = Sha256.hash(AESforInfoKeyEncyption);
        byte[] AESforBtcPrivKey = Sha256.hash(ByteUtils.bytesToHex(seed));

        System.out.println("Provide the Electrum wallet password (hex string):");
        String btcPrivKey = (new Scanner(System.in)).nextLine().trim();

        byte[] encryptedPrivKey = AES.encrypt(ByteUtils.hexToBytes(btcPrivKey), AESforBtcPrivKey);

        FileHandler handler = new FileHandler(fName, 100);

        int position = handler.putElementInRandomPlace(encryptedPrivKey);

        byte[] infoKey = new byte[36];
        System.arraycopy(ByteUtils.unsignedToBytes(position), 0, infoKey, 0, 4);
        System.arraycopy(AESforBtcPrivKey, 0, infoKey, 4, 32);

        Properties.setProperty(infoKeyEncrypted, AES.encrypt(infoKey, AESforInfoKeyEncyption));

        System.out.println("Keys creation finished. Complete configuration on exchange side now.");
    }
}
