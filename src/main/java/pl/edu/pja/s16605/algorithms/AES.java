package pl.edu.pja.s16605.algorithms;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class AES {

    private static SecretKeySpec secretKey;

    /**
     * Sets secret key to encrypt or decrypt elements
     *
     * @param initKey initial information for key creation
     * @throws NoSuchAlgorithmException
     */
    public static void setKey(byte[] initKey) throws NoSuchAlgorithmException {
        secretKey = new SecretKeySpec(Sha256.hash(initKey), "AES");
    }

    /**
     * Encrypts element using AES algorithm
     *
     * @param element element to be encrypted
     * @param secret  initial information for key creation
     * @return encrypted element
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     */
    public static byte[] encrypt(byte[] element, byte[] secret) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return cipher.doFinal(element);
    }

    /**
     * Decrypts element using AES algorithm
     *
     * @param toDecrypt element to be decrypted
     * @param secret    initial information for key creation
     * @return decrypted element
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] decrypt(byte[] toDecrypt, byte[] secret) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        setKey(secret);
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return cipher.doFinal(toDecrypt);
    }
}