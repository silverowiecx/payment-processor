package pl.edu.pja.s16605.algorithms;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Container for hashing methods
 */
public class Sha256 {


    /**
     * Encrypts any string
     *
     * @param toHash String to be hashed
     * @return 32 byte array of hash
     */
    public static byte[] hash(String toHash) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        return messageDigest.digest(toHash.getBytes());
    }

    /**
     * Encrypts any byte array
     *
     * @param toHash byte array to be hashed
     * @return 32 byte array of hash
     */
    public static byte[] hash(byte[] toHash) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        return messageDigest.digest(toHash);
    }

}