package pl.edu.pja.s16605.algorithms;

import javax.crypto.Cipher;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSA {

    /**
     * Generates public and private key
     *
     * @return KeyPair object
     * @throws Exception
     */
    public static KeyPair generateKeyPair() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048, new SecureRandom());
        return generator.generateKeyPair();
    }


    /**
     * Encrypts byte array with provided PublicKey
     *
     * @param element   array to be encrypted
     * @param publicKeyArray Public key to be used for encryption
     * @return encrypted byte array
     * @throws Exception
     */
    public static byte[] encrypt(byte[] element, byte[] publicKeyArray) throws Exception {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = factory.generatePublic(new X509EncodedKeySpec(publicKeyArray));
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return encryptCipher.doFinal(element);
    }

    /**
     * Decrypts byte array with provided PrivateKey
     *
     * @param element    byte array to be decrypted
     * @param privateKeyArray Private key to be used for decryption
     * @return decrypted byte array
     * @throws Exception
     */
    public static byte[] decrypt(byte[] element, byte[] privateKeyArray) throws Exception {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = factory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyArray));
        Cipher decryptCipher = Cipher.getInstance("RSA");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        return decryptCipher.doFinal(element);
    }


}
