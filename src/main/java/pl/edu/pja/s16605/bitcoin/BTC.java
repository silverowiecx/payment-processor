package pl.edu.pja.s16605.bitcoin;


import org.json.JSONObject;
import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;
import pl.edu.pja.s16605.exceptions.BtcExceptions.ElectrumException;
import pl.edu.pja.s16605.exceptions.BtcExceptions.NotEnoughFeeException;
import pl.edu.pja.s16605.utils.ByteUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static pl.edu.pja.s16605.utils.Log.log;
import static pl.edu.pja.s16605.utils.Log.logError;

public class BTC {

    public final static double satoshi = 100000000;

    private static String path_to_electrum = "electrum";
    private static boolean testnet = true;


    /**
     * Executes command in electrum application
     *
     * @param argv command and arguments to execute
     * @return result of the command execution
     */
    public static String executeCommand(String... argv) {
        String[] args;

        if (testnet) {
            args = new String[argv.length + 2];
            args[1] = "--testnet";
            System.arraycopy(argv, 0, args, 2, argv.length);
        } else {
            args = new String[argv.length + 1];
            System.arraycopy(argv, 0, args, 1, argv.length);
        }

        args[0] = path_to_electrum;

        System.out.println(Arrays.toString(args));

        ProcessBuilder processBuilder = new ProcessBuilder(args);

        try {
            Process process = processBuilder.start();

            InputStream inputStream = process.getInputStream();

            String result = new String(inputStream.readAllBytes());
            int exitCode = process.waitFor();

            log("Electrum command execution exit code: " + exitCode);

            return result;

        } catch (Exception e) {
            logError(e.getMessage());
        }

        return null;

    }


    /**
     * Gets confirmed balance of the wallet
     *
     * @return confirmed balance
     * @throws ElectrumException
     */
    public static double getBalance() throws ElectrumException {
        String result = executeCommand("getbalance");
        if (result != null) {
            JSONObject jsonObject = new JSONObject(result);
            return Double.parseDouble(jsonObject.getString("confirmed"));
        } else throw new ElectrumException();
    }

    /**
     * Sends funds to one recipient
     *
     * @param address recipient's address
     * @param amount  amount to be sent
     * @param maxFee  maximum fee for transaction
     * @param key     password to electrum wallet
     * @return transaction hash
     * @throws ElectrumException
     * @throws NotEnoughFeeException
     */
    public static String payTo(String address, long amount, long maxFee, byte[] key) throws ElectrumException, NotEnoughFeeException {
        String amountStr = String.format("%.8f", (double) amount / satoshi);
        double maxFeeDouble = (double) maxFee / satoshi;
        String signed = executeCommand("payto", address, amountStr, "--password", ByteUtils.bytesToHex(key));
        return executeTransaction(signed, maxFeeDouble);
    }


    /**
     * Sends funds to many recipients
     *
     * @param requests list of payment requests
     * @param key      password to electrum wallet
     * @return transaction hash
     * @throws NotEnoughFeeException
     * @throws ElectrumException
     */
    public static String payToMany(ArrayList<BtcTransactionRequest> requests, byte[] key) throws NotEnoughFeeException, ElectrumException {
        String signed = executeCommand("paytomany", "--password", ByteUtils.bytesToHex(key), parseRequests(requests));
        double maxFeeDouble = sumMaxFee(requests);
        return executeTransaction(signed, maxFeeDouble);

    }

    /**
     * Broadcasts transaction if fee is sufficient
     *
     * @param signed       signed transaction
     * @param maxFeeDouble boundary amount of funds to be used as a fee
     * @return transaction hash
     * @throws ElectrumException
     * @throws NotEnoughFeeException
     */
    private static String executeTransaction(String signed, double maxFeeDouble) throws ElectrumException, NotEnoughFeeException {
        String txHex;
        double txSize;
        if (signed != null) {
            txHex = new JSONObject(signed).getString("hex");
            txSize = txHex.getBytes().length;
        } else throw new ElectrumException();
        if (txSize * estimateFee() <= maxFeeDouble) {
            return executeCommand("broadcast", new JSONObject(signed).getString("hex"));
        } else throw new NotEnoughFeeException();
    }

    /**
     * Sums fees that can be used for transaction
     *
     * @param requests list of payment requests
     * @return sum of maxFees from requests
     */
    private static double sumMaxFee(ArrayList<BtcTransactionRequest> requests) {
        return (double) requests.stream().mapToLong(r -> r.maxFee).sum() / satoshi;
    }


    /**
     * Parses payment requests to form of electrum command
     *
     * @param requests list of payment requests
     * @return "outputs" argument for electrum
     */
    public static String parseRequests(ArrayList<BtcTransactionRequest> requests) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");

        for (BtcTransactionRequest request : requests) {
            String amountStr = String.format("%.8f", (double) request.amount / satoshi);
            String address = request.address;
            builder.append("[\"").append(address).append("\", \"").append(amountStr).append("\"], ");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.deleteCharAt(builder.length() - 1);
        builder.append("]");
        return builder.toString();
    }


    /**
     * Gets fee per byte of transaction
     *
     * @return fee per byte (in satoshis)
     * @throws ElectrumException
     */
    private static double estimateFee() throws ElectrumException {
        String result = executeCommand("getfeerate");
        double satPerKvByte;
        if (result != null)
            satPerKvByte = Double.parseDouble(result);
        else
            throw new ElectrumException();
        return satPerKvByte / 1000 / satoshi;
    }


}
