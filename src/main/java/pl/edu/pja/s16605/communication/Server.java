package pl.edu.pja.s16605.communication;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import static pl.edu.pja.s16605.utils.Log.log;
import static pl.edu.pja.s16605.utils.Log.logError;

public class Server extends Thread {


    public boolean running;
    public Client client;
    public ServerConfig config;
    private ServerSocket server;

    /**
     * Creates server with given ServerConfig object
     *
     * @param config ServerConfig object with server parameters
     * @throws IOException
     */
    public Server(ServerConfig config) throws IOException {
        this.config = config;
        server = new ServerSocket(this.config.port);
    }

    /**
     * Makes the server waiting for connections
     */
    void listen() {
        while (running) {
            try {
                server.setSoTimeout(1000);
                Socket tcpClient = server.accept();
                this.client = new Client(tcpClient, this);
                this.client.start();
                log("New incoming connection.");
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * Connects to a known node
     *
     * @throws IOException
     */
    public void connect() throws IOException {
        if (client == null) {
            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(config.knownConfig.IP, config.knownConfig.port), 5000);
                this.client = new Client(socket, this);
                this.client.start();
                log("Connected successfully to: " + config);
            } catch (IOException e) {
                logError("Could not connect to: " + config);
            }
        }
    }


    @Override
    public void run() {
        try {
            running = true;
            listen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Close running server
     *
     * @throws Exception
     */
    public void close() throws Exception {
        running = false;
        client.close();
    }


}
