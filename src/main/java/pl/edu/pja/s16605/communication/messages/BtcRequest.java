package pl.edu.pja.s16605.communication.messages;

import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class BtcRequest extends Message {

    public ArrayList<BtcTransactionRequest> requests;  // List of bitcoin transfer requests


    /**
     * Constructor used to receive bitcoin transfer requests message
     *
     * @param inputStream stream with raw data of message
     * @throws Exception
     */
    public BtcRequest(InputStream inputStream, String rsaPrivType) throws Exception {
        super(MessageType.BtcRequest, inputStream, rsaPrivType);
    }


    /**
     * Constructor used create message with bitcoin transfer requests
     *
     * @param timestamp moment of creating a message
     * @param nonce     additional message identifier
     * @param key       key used to decrypt bitcoin's privkey
     * @param requests  list of bitcoin transfer requests
     */
    public BtcRequest(long timestamp, int nonce, byte[] key, ArrayList<BtcTransactionRequest> requests) {
        super(MessageType.BtcRequest, timestamp, nonce, key);
        this.requests = requests;
    }

    /**
     * Deserializes bitcoin transfer requests from raw data
     *
     * @param bytes data of a message
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    protected void deserializeData(byte[] bytes) throws IOException, ClassNotFoundException {
        requests = (ArrayList<BtcTransactionRequest>) (new ObjectInputStream(new ByteArrayInputStream(bytes))).readObject();
    }


    /**
     * Turns BtcTransactionRequest list into raw bytes
     *
     * @return serialized list of requests
     * @throws IOException
     */
    @Override
    protected byte[] serializeData() throws IOException {
        ByteArrayOutputStream memoryStream = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(memoryStream);
        out.writeObject(requests);
        out.flush();
        return memoryStream.toByteArray();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BtcRequest request = (BtcRequest) o;
        return Objects.equals(requests, request.requests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), requests);
    }

    @Override
    public String toString() {
        return "BtcRequest{" +
                "requests=" + requests +
                ", messageType=" + messageType +
                ", length=" + length +
                ", timestamp=" + timestamp +
                ", nonce=" + nonce +
                ", datalen=" + data.length +
                ", data=" + Arrays.toString(data) +
                ", key=" + Arrays.toString(key) +
                ", inputStream=" + inputStream +
                '}';
    }
}
