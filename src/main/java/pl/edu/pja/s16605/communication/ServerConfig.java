package pl.edu.pja.s16605.communication;

import pl.edu.pja.s16605.algorithms.Sha256;
import pl.edu.pja.s16605.utils.ByteUtils;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;


public class ServerConfig {
    /**
     * Server configuration
     */
    public Role role;                         // Node role
    public String IP;                         // External IP address of connection
    public int port;                          // External port of connection
    public byte[] hash;                       // ID hash
    public ServerConfig knownConfig;                 // Known config of exchange/processor

    /**
     * Constructor taking configuration parameters
     *
     * @param hash ID of server
     * @param role role of the server (Exchange, Processor)
     * @param IP   node IP that is allowed to be connected with
     * @param port node port number that is allowed to be connected with
     */
    public ServerConfig(byte[] hash, Role role, String IP, int port) {
        this.hash = hash;
        this.role = role;
        this.IP = IP;
        this.port = port;
    }

    /**
     * Constructor taking configuration parameters
     *
     * @param toHash ID of server (not hashed)
     * @param role   role of the server (Exchange, Processor)
     * @param IP     node IP that is allowed to be connected with
     * @param port   node port number that is allowed to be connected with
     */
    public ServerConfig(String toHash, Role role, String IP, int port) throws NoSuchAlgorithmException {
        this(Sha256.hash(toHash), role, IP, port);
    }

    @Override
    public String toString() {
        return IP + ":" + port + " ID: " + ByteUtils.bytesToHex(hash);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServerConfig that = (ServerConfig) o;
        return port == that.port &&
                role == that.role &&
                Objects.equals(IP, that.IP) &&
                Arrays.equals(hash, that.hash);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(role, IP, port);
        result = 31 * result + Arrays.hashCode(hash);
        return result;
    }

    /**
     * Role of a Node
     */
    public enum Role {
        Processor,
        Exchange
    }
}

