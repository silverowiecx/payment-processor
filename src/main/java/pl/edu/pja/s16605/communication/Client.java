package pl.edu.pja.s16605.communication;

import pl.edu.pja.s16605.communication.messages.BtcRequest;
import pl.edu.pja.s16605.communication.messages.BtcResponse;
import pl.edu.pja.s16605.communication.messages.Message;
import pl.edu.pja.s16605.communication.messages.MessageType;
import pl.edu.pja.s16605.exceptions.*;
import pl.edu.pja.s16605.processors.BtcExchange;
import pl.edu.pja.s16605.processors.BtcProcessor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static pl.edu.pja.s16605.utils.ByteUtils.bytesToHex;
import static pl.edu.pja.s16605.utils.Log.log;
import static pl.edu.pja.s16605.utils.Log.logError;


public class Client extends Thread {
    byte[] connectedTo;
    /**
     * Client class that handles connections
     */

    private DataInputStream reader;
    private DataOutputStream writer;
    private Server server;
    private Socket client;

    /**
     * Constructor that starts communication between connected clients
     *
     * @param client Client to be used
     */
    public Client(Socket client, Server server) throws IOException {
        this.client = client;
        this.server = server;
        writer = new DataOutputStream(client.getOutputStream());
        reader = new DataInputStream(client.getInputStream());
    }

    /**
     * Initialize connection
     */
    public void connect() {
        try {
            writer.write(server.config.hash);
            writer.flush();

            byte[] hash = new byte[32];
            reader.read(hash);

            if (Arrays.equals(server.config.knownConfig.hash, hash)) {
                connectedTo = hash;
                log("Authenticated with: " + bytesToHex(connectedTo));
                processMessages();
            } else close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close connection
     */
    void close() {
        try {
            client.close();
            server.client = null;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Sends a message to the client
     *
     * @param message message to be sent
     * @return true if completed successfully
     * @throws Exception
     */
    public boolean sendMessage(Message message) throws Exception {
        if (!MessageType.checkField(message.messageType))
            throw new IllegalMessageTypeException(message.messageType);
        if (server.config.role == ServerConfig.Role.Exchange) {
            if (message.messageType >= MessageType.BtcRequest) {
                message.serialize(writer, String.valueOf(message.messageType) + "ERP");
                writer.flush();
                return true;
            } else throw new InvalidMessageTypeException(message.messageType);
        } else {
            if (message.messageType < MessageType.BtcRequest) {
                message.serialize(writer, String.valueOf(message.messageType) + "PRP");
                writer.flush();
                return true;
            } else throw new InvalidMessageTypeException(message.messageType);
        }
    }


    /**
     * Response for messages from connected Node
     */
    private void processMessages() {
        byte[] buffer;
        while (!client.isClosed()) {
            try {
                buffer = new byte[1];
                reader.read(buffer);
                switch (buffer[0]) {
                    case MessageType.BtcRequest:
                        BtcRequest btcRequest = new BtcRequest(reader, String.valueOf(MessageType.BtcRequest) + "PRR");
                        if (processBtcRequest(btcRequest))
                            break;
                        else throw new IllegalClientStateException();
                    case MessageType.BtcResponse:
                        BtcResponse btcResponse = new BtcResponse(reader, String.valueOf(MessageType.BtcResponse) + "ERR");
                        if (processBtcResponse(btcResponse))
                            break;
                        else throw new IllegalClientStateException();
                    default:
                        logError("I read something ridiculous: " + Arrays.toString(buffer));
                        logError("Closing this client...");
                        throw new IllegalClientStateException();
                }
                sleep(20);
            } catch (Exception e) {
                e.printStackTrace();
                while (!this.isInterrupted())
                    this.interrupt();
                close();
                return;
            }
        }
    }

    /**
     * Redirect BtcResponse message to the exchange
     *
     * @param btcResponse response to be forwarded
     * @throws NoSuchAlgorithmException
     * @throws InvalidResponseException
     * @throws InvalidPropertyException
     */
    private boolean processBtcResponse(BtcResponse btcResponse) throws NoSuchAlgorithmException, InvalidResponseException, InvalidPropertyException {
        log("Received response: " + btcResponse);
        return ((BtcExchange) server).processResponse(btcResponse);
    }

    /**
     * Redirect BtcRequest message to the processor
     *
     * @param btcRequest request to be forwarded
     * @throws Exception
     */
    private boolean processBtcRequest(BtcRequest btcRequest) throws Exception {
        log("Received request: " + btcRequest);
        return ((BtcProcessor) server).receiveRequest(btcRequest);
    }

    /**
     * Start the Client thread
     */
    @Override
    public void run() {
        connect();
    }


}
