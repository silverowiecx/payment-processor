package pl.edu.pja.s16605.communication.messages;

import pl.edu.pja.s16605.Properties;
import pl.edu.pja.s16605.algorithms.AES;
import pl.edu.pja.s16605.algorithms.RSA;
import pl.edu.pja.s16605.utils.ByteUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;

import static pl.edu.pja.s16605.utils.ByteUtils.switchEndianness;

public abstract class Message {

    /*
    1. type, AES
    2. length
    3. timestamp
    4. key
    5. listening port
    6. address list, amount, fee (fixed to 10 requests or time-bounded)
    */


    public byte messageType;                   // Type of message
    long length;                        // Length of transferred data
    public long timestamp;                     // Timestamp (moment of message creation)
    public int nonce;                           // Port used for listening to response
    byte[] data;                        // Encrypted data to be transferred
    byte[] aesKey;                      // Key used to encrypt data
    public byte[] key;                         // Key used for decrypting cryptocurrency's privkey
    ByteArrayInputStream inputStream;   // Helper stream to process message data


    /**
     * Constructor used for creating message
     *
     * @param messageType type of the message
     * @param timestamp   moment of message creation
     * @param nonce       additional message identifier
     * @param key         used for decrypting cryptocurrency's privkey
     */
    public Message(byte messageType, long timestamp, int nonce, byte[] key) {
        this.messageType = messageType;
        this.timestamp = timestamp;
        this.nonce = nonce;
        this.aesKey = new byte[32];
        new SecureRandom().nextBytes(aesKey);
        this.key = key;
    }

    /**
     * Constructor used for receiving messages
     *
     * @param messageType type of the message
     * @param inputStream stream with raw message data
     * @throws Exception
     */
    public Message(byte messageType, InputStream inputStream, String rsaPrivType) throws Exception {
        this.messageType = messageType;

        byte[] buffer = new byte[256];
        inputStream.read(buffer);
        aesKey = switchEndianness(buffer);
        aesKey = RSA.decrypt(aesKey, Properties.getProperty(rsaPrivType));

        buffer = new byte[4];
        inputStream.read(buffer);
        length = ByteUtils.bytesToUnsigned(switchEndianness(buffer));

        data = new byte[(int)length]; // no problem, request won't be bigger than 2GiB
        inputStream.read(data);
        data = AES.decrypt(data, aesKey);
        //  aesKey = null;

        this.inputStream = new ByteArrayInputStream(data);

        buffer = new byte[4];
        this.inputStream.read(buffer);
        timestamp = ByteUtils.bytesToUnsigned(switchEndianness(buffer));

        buffer = new byte[32];
        this.inputStream.read(buffer);
        key = switchEndianness(buffer);

        buffer = new byte[4];
        this.inputStream.read(buffer);
        nonce = (int) ByteUtils.bytesToUnsigned(switchEndianness(buffer));

        deserializeData(this.inputStream.readAllBytes());
    }

    /**
     * Deserializes raw data into a message
     *
     * @param bytes data of a message
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected abstract void deserializeData(byte[] bytes) throws IOException, ClassNotFoundException;

    /**
     * Serializes message into output stream
     *
     * @param outputStream stream used for receiving raw data of message
     * @throws Exception
     */
    public void serialize(OutputStream outputStream, String rsaPubType) throws Exception {
        byte[] serializedTransactions = serializeData();
        /*
        1. timestamp
        2. key
        3. port
        4. data
        */
        if (aesKey.length == 32) {
            data = new byte[4 + 32 + 4 + serializedTransactions.length]; // msgType+timestamp+key+port+data

            System.arraycopy(ByteUtils.switchEndianness(ByteUtils.unsignedToBytes(timestamp)), 0, data, 0, 4);
            System.arraycopy(switchEndianness(key), 0, data, 4, 32);
            System.arraycopy(switchEndianness(ByteUtils.unsignedToBytes(nonce)), 0, data, 36, 4);
            System.arraycopy(serializedTransactions, 0, data, 40, serializedTransactions.length);

            data = AES.encrypt(data, aesKey);

            aesKey = RSA.encrypt(aesKey, Properties.getProperty(rsaPubType));
        }
        this.length = data.length;

        outputStream.write(messageType);
        outputStream.write(switchEndianness(aesKey));
        outputStream.write(switchEndianness(ByteUtils.unsignedToBytes(length)));
        outputStream.write(data);
    }

    /**
     * Serializes significant part of message to raw data
     *
     * @return raw data of message part as array of bytes
     * @throws IOException
     */
    abstract protected byte[] serializeData() throws IOException;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return messageType == message.messageType &&
                timestamp == message.timestamp &&
                nonce == message.nonce &&
                Arrays.equals(key, message.key);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(messageType, length, timestamp, nonce);
        result = 31 * result + Arrays.hashCode(aesKey);
        result = 31 * result + Arrays.hashCode(key);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageType=" + messageType +
                ", length=" + length +
                ", timestamp=" + timestamp +
                ", nonce=" + nonce +
                ", datalen=" + data.length +
                ", data=" + Arrays.toString(data) +
                ", key=" + Arrays.toString(key) +
                ", inputStream=" + inputStream +
                '}';
    }
}
