package pl.edu.pja.s16605.communication.messages;

import java.lang.reflect.Field;

public class MessageType {

    /**
     * Set of possible message headers
     */

    public static final byte BtcResponse = 1;  // message header of bitcoin transfer requests
    public static final byte BtcRequest = 64;   // message header of bitcoin transfer response


    /**
     * Checks if type of message is declared
     *
     * @param field type of message to be checked
     * @return true if type is declared, false otherwise
     * @throws IllegalAccessException
     */
    public static boolean checkField(byte field) throws IllegalAccessException {
        for (Field f : MessageType.class.getDeclaredFields())
            if (f.getByte(MessageType.class) == field)
                return true;
        return false;
    }

}
