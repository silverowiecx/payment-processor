package pl.edu.pja.s16605.communication.messages;

import pl.edu.pja.s16605.utils.ByteUtils;

import java.io.InputStream;
import java.util.Arrays;

public class BtcResponse extends Message {

    public String txData; // i.e. transaction hash

    /**
     * Constructor used for creating a response with bitcoin tx hash
     *
     * @param txData bitcoin tx hash - hex String
     */
    public BtcResponse(String txData, int nonce) {
        super(MessageType.BtcResponse, 0, nonce, new byte[32]);
        this.txData = txData;
    }


    /**
     * Constructor used to receive a response
     *
     * @param inputStream stream with raw data of message
     * @throws Exception
     */
    public BtcResponse(InputStream inputStream, String rsaPrivType) throws Exception {
        super(MessageType.BtcResponse, inputStream, rsaPrivType);
    }

    /**
     * Converts bytes to hex string
     *
     * @param bytes data of a message
     */
    @Override
    protected void deserializeData(byte[] bytes) {
        txData = ByteUtils.bytesToHex(ByteUtils.switchEndianness(bytes));
    }

    /**
     * Converts hex string to bytes
     *
     * @return
     */
    @Override
    protected byte[] serializeData() {
        return ByteUtils.switchEndianness(ByteUtils.hexToBytes(txData));
    }


    @Override
    public String toString() {
        return "BtcResponse{" +
                "txData='" + txData + '\'' +
                ", messageType=" + messageType +
                ", length=" + length +
                ", timestamp=" + timestamp +
                ", nonce=" + nonce +
                ", data=" + Arrays.toString(data) +
                ", aesKey=" + Arrays.toString(aesKey) +
                ", key=" + Arrays.toString(key) +
                ", inputStream=" + inputStream +
                '}';
    }
}
