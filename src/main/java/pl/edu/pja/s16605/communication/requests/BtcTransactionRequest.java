package pl.edu.pja.s16605.communication.requests;

import java.io.Serializable;
import java.util.Objects;

public class BtcTransactionRequest implements Serializable {
    private static final long serialVersionUID = 3883249360817124509L;

    public String address; // address of recipient
    public long amount;    // amount of cryptocurrency to be sent
    public long maxFee;    // maximum amount of cryptocurrency to be used as a miners' fee

    /**
     * Default constructor (because it implements Serializable)
     */
    public BtcTransactionRequest() {
    }

    /**
     * Constructor used to create request
     *
     * @param address address of recipient
     * @param amount  amount of cryptocurrency to be sent
     * @param maxFee  maximum amount of cryptocurrency to be used as a miners' fee
     */
    public BtcTransactionRequest(String address, long amount, long maxFee) {
        this.address = address;
        this.amount = amount;
        this.maxFee = maxFee;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BtcTransactionRequest that = (BtcTransactionRequest) o;
        return amount == that.amount &&
                maxFee == that.maxFee &&
                address.equals(that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, amount, maxFee);
    }

    @Override
    public String toString() {
        return "BtcTransactionRequest{" +
                "address='" + address + '\'' +
                ", amount=" + amount +
                ", maxFee=" + maxFee +
                '}';
    }
}
