package pl.edu.pja.s16605;

import pl.edu.pja.s16605.communication.messages.MessageType;
import pl.edu.pja.s16605.utils.ByteUtils;

import java.util.Scanner;

public class ExchangeInit {

    public static String halfKey32Exchange = String.valueOf(MessageType.BtcResponse) + "HK";

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first key:");
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "ERP",
                ByteUtils.hexToBytes(scanner.nextLine().trim()));
        System.out.println("Enter the second key:");
        Properties.setProperty(String.valueOf(MessageType.BtcResponse) + "ERR", ByteUtils.hexToBytes(scanner.nextLine().trim()));
        System.out.println("Enter the third key:");
        Properties.setProperty(halfKey32Exchange, ByteUtils.hexToBytes(scanner.nextLine().trim()));

        System.out.println("Keys imported. If they are proper, exchange side is ready to use.");
    }
}
