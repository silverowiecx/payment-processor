package pl.edu.pja.s16605.processors;

import pl.edu.pja.s16605.FileHandler;
import pl.edu.pja.s16605.Properties;
import pl.edu.pja.s16605.algorithms.AES;
import pl.edu.pja.s16605.algorithms.Sha256;
import pl.edu.pja.s16605.bitcoin.BTC;
import pl.edu.pja.s16605.communication.Server;
import pl.edu.pja.s16605.communication.ServerConfig;
import pl.edu.pja.s16605.communication.messages.BtcRequest;
import pl.edu.pja.s16605.communication.messages.BtcResponse;
import pl.edu.pja.s16605.communication.messages.MessageType;
import pl.edu.pja.s16605.exceptions.IllegalRequestException;
import pl.edu.pja.s16605.exceptions.InvalidPropertyException;
import pl.edu.pja.s16605.utils.ByteUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static pl.edu.pja.s16605.utils.Log.logError;


public class BtcProcessor extends Server {

    private FileHandler fileHandler;
    private BtcRequest request;         // current request to be processed


    /**
     * Processor class for processing messages with orders for withdrawals
     *
     * @param config   server configuration
     * @param fName    name of the file with hidden private key
     * @param fileSize size of the file with hidden private key
     * @throws Exception
     */
    public BtcProcessor(ServerConfig config, String fName, int fileSize) throws Exception {
        super(config);
        this.fileHandler = new FileHandler(fName, fileSize);
        this.start();
    }

    /**
     * Wait for previous task completion and start processing a new one
     *
     * @param request request to be processed
     * @throws Exception
     */
    public boolean receiveRequest(BtcRequest request) throws Exception {
        while (this.request != null) {
            sleep(300);
        }
        this.request = request;
        processRequest();
        return true;
    }


    /**
     * Decrypts key, executes payment requests and sends confirmation to the exchange
     *
     * @throws Exception
     */
    private void processRequest() throws Exception {
        byte[] btcPrivKey = decryptKey();

        logError("Decrypted privkey: " + ByteUtils.bytesToHex(btcPrivKey));

        /*
        Here should be implementation of preparing and broadcasting Bitcoin transaction with result of transaction hash
         */

        String txId = BTC.payToMany(request.requests, btcPrivKey);
        encryptStoreInfoKey(encryptKey(btcPrivKey));

        client.sendMessage(new BtcResponse(txId, request.nonce)); // send response


        request = null;
    }


    /**
     * Parses two parts of key to encrypted infoKey
     *
     * @return Sha-256 sum of parsed array
     * @throws InvalidPropertyException
     * @throws NoSuchAlgorithmException
     */
    private byte[] parseKey() throws InvalidPropertyException, NoSuchAlgorithmException {
        byte[] parsed = new byte[64];
        System.arraycopy(request.key, 0, parsed, 0, 32);
        System.arraycopy(Properties.getProperty(String.valueOf(MessageType.BtcRequest) + "HK"), 0, parsed, 32, 32);
        return Sha256.hash(parsed);
    }

    /**
     * Gets key from noisy file and decrypts it
     *
     * @return decrypted private key
     * @throws Exception
     */
    private byte[] decryptKey() throws Exception {
        byte[] infoKey = AES.decrypt(Properties.getProperty(String.valueOf(MessageType.BtcRequest) + "IK"), parseKey());
        byte[] key = new byte[32];
        byte[] pos = new byte[4];
        System.arraycopy(infoKey, 0, pos, 0, 4);
        System.arraycopy(infoKey, 4, key, 0, 32);
        long position = 0;
        try {
            position = ByteUtils.bytesToUnsigned(pos);
            ByteUtils.bytesToHex(key);
        } catch (Exception e) {
            throw new IllegalRequestException("Key or position is invalid!");
        }
        if (position < 0)
            throw new IllegalRequestException("Position is negative!");
        byte[] encryptedElement = fileHandler.getElement(position, 48);


        return AES.decrypt(encryptedElement, key);
    }

    /**
     * Encrypts private key and puts it in noisy file
     *
     * @param decryptedElement element to be encrypted and hidden
     * @return returns infoKey with information of key position in file and key to decrypt it
     * @throws Exception
     */
    private byte[] encryptKey(byte[] decryptedElement) throws Exception {
        byte[] keyToEncrypt = new byte[32];
        new SecureRandom().nextBytes(keyToEncrypt);
        byte[] encryptedElement = AES.encrypt(decryptedElement, keyToEncrypt);

        int pos = fileHandler.putElementInRandomPlace(encryptedElement);
        byte[] position = ByteUtils.unsignedToBytes(pos);

        byte[] infoKey = new byte[36];
        System.arraycopy(position, 0, infoKey, 0, 4);
        System.arraycopy(keyToEncrypt, 0, infoKey, 4, 32);

        return infoKey;
    }

    /**
     * Hashes a part of parsedKey
     *
     * @param received hashes received part when true, else owned
     * @return Sha-256 sum of part of the key
     * @throws NoSuchAlgorithmException
     * @throws InvalidPropertyException
     */
    private byte[] hashPartKey(boolean received) throws NoSuchAlgorithmException, InvalidPropertyException {
        byte[] toHash = new byte[40];
        System.arraycopy(ByteUtils.unsignedToBytes(request.timestamp), 0, toHash, 0, 4);
        if (received)
            System.arraycopy(request.key, 0, toHash, 4, 32);
        else
            System.arraycopy(Properties.getProperty(String.valueOf(MessageType.BtcRequest) + "HK"), 0, toHash, 4, 32);
        System.arraycopy(ByteUtils.unsignedToBytes(request.nonce), 0, toHash, 36, 4);
        return Sha256.hash(toHash);
    }

    private byte[] hashReceivedKey() throws InvalidPropertyException, NoSuchAlgorithmException {
        return hashPartKey(true);
    }

    private byte[] hashOwnedKey() throws InvalidPropertyException, NoSuchAlgorithmException {
        byte[] newOwned = hashPartKey(false);
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "HK", newOwned);
        return newOwned;
    }

    /**
     * Introduce new key from new key parts
     *
     * @return Sha-256 sum of new key
     * @throws InvalidPropertyException
     * @throws NoSuchAlgorithmException
     */
    private byte[] parseNewKey() throws InvalidPropertyException, NoSuchAlgorithmException {
        byte[] twoKeys = new byte[64];
        System.arraycopy(hashReceivedKey(), 0, twoKeys, 0, 32);
        System.arraycopy(hashOwnedKey(), 0, twoKeys, 32, 32);
        return Sha256.hash(twoKeys);
    }

    /**
     * Encrypts and stores infoKey in application preferences
     *
     * @param infoKey key to be stored and encrypted
     * @throws InvalidPropertyException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws NoSuchPaddingException
     * @throws BadPaddingException
     */
    private void encryptStoreInfoKey(byte[] infoKey) throws InvalidPropertyException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, BadPaddingException {
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "IK", AES.encrypt(infoKey, parseNewKey()));
    }


}
