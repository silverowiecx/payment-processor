package pl.edu.pja.s16605.processors;

import pl.edu.pja.s16605.Properties;
import pl.edu.pja.s16605.algorithms.Sha256;
import pl.edu.pja.s16605.communication.Server;
import pl.edu.pja.s16605.communication.ServerConfig;
import pl.edu.pja.s16605.communication.messages.BtcRequest;
import pl.edu.pja.s16605.communication.messages.BtcResponse;
import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;
import pl.edu.pja.s16605.exceptions.InvalidPropertyException;
import pl.edu.pja.s16605.exceptions.InvalidResponseException;
import pl.edu.pja.s16605.utils.ByteUtils;
import pl.edu.pja.s16605.utils.Timestamp;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import static pl.edu.pja.s16605.Main.halfKey32Exchange;
import static pl.edu.pja.s16605.utils.Log.logError;


public class BtcExchange extends Server {

    private BtcRequest request;                         // Current processed message
    public ArrayList<BtcTransactionRequest> requests;  // List of transaction requests to be sent to Processor


    /**
     * Exchange class for processing orders for withdrawals
     *
     * @param config server configuration
     * @throws Exception
     */
    public BtcExchange(ServerConfig config) throws Exception {
        super(config);
        requests = new ArrayList<>();
    }


    /**
     * Queue a withdrawal request to be sent
     *
     * @param request transaction request to be sent
     * @throws Exception
     */
    public boolean addRequest(BtcTransactionRequest request) throws Exception {
        requests.add(request);
        if (requests.size() == 10)
            sendTransactionRequests();
        return true;
    }

    /**
     * Prepare a message and send it to the processor
     *
     * @throws Exception
     */
    public boolean sendTransactionRequests() throws Exception {
        while (request != null)
            sleep(1000);
        request = new BtcRequest(Timestamp.get(), Math.abs(new SecureRandom().nextInt()),
                Properties.getProperty(halfKey32Exchange), requests);
        client.sendMessage(request);
        requests.clear();
        return true;
    }


    /**
     * Inform users of successful transaction and refresh the key
     *
     * @param response received response
     * @return
     * @throws InvalidResponseException
     * @throws InvalidPropertyException
     * @throws NoSuchAlgorithmException
     */
    public boolean processResponse(BtcResponse response) throws InvalidResponseException, InvalidPropertyException, NoSuchAlgorithmException {
        if (response.nonce != request.nonce)
            throw new InvalidResponseException();

        String transactionConfirmation = response.txData;

        logError("Received transaction confirmation of hash: " + transactionConfirmation);
        /*
        Broadcast transaction confirmation to user(s).
        I.e. create a method that associates users with their cryptocurrency addresses present in requests
             and send txid to them.
         */

        refreshKey();

        request = null;
        return true;
    }


    /**
     * Refresh key with new parameters
     *
     * @throws NoSuchAlgorithmException
     * @throws InvalidPropertyException
     */
    private void refreshKey() throws NoSuchAlgorithmException, InvalidPropertyException {
        byte[] toHash = new byte[40];
        System.arraycopy(ByteUtils.unsignedToBytes(request.timestamp), 0, toHash, 0, 4);
        System.arraycopy(Properties.getProperty(halfKey32Exchange), 0, toHash, 4, 32);
        System.arraycopy(ByteUtils.unsignedToBytes(request.nonce), 0, toHash, 36, 4);
        Properties.setProperty(halfKey32Exchange, Sha256.hash(toHash));
    }

}
