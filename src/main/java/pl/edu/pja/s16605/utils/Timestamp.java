package pl.edu.pja.s16605.utils;


/**
 * Container for Timestamp methods
 */
public class Timestamp {
    /**
     * Gets current Timestamp.
     * In production version to be replaced by timestamping system.
     *
     * @return 4 byte array timestamp
     */
    public static long get() {
        return System.currentTimeMillis() / 1000;
    }
}