package pl.edu.pja.s16605;


import pl.edu.pja.s16605.algorithms.AES;
import pl.edu.pja.s16605.algorithms.RSA;
import pl.edu.pja.s16605.algorithms.Sha256;
import pl.edu.pja.s16605.communication.ServerConfig;
import pl.edu.pja.s16605.communication.messages.MessageType;
import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;
import pl.edu.pja.s16605.processors.BtcExchange;
import pl.edu.pja.s16605.processors.BtcProcessor;
import pl.edu.pja.s16605.utils.ByteUtils;

import java.security.KeyPair;


public class Main {

    // some variables for testing purposes:
    // Processor:
    public static String fName = String.valueOf(MessageType.BtcRequest) + "K";
    public static String infoKeyEncrypted = String.valueOf(MessageType.BtcRequest) + "IK";
    public static String halfKey32Processor = String.valueOf(MessageType.BtcRequest) + "HK";

    // Exchange:
    public static String halfKey32Exchange = String.valueOf(MessageType.BtcResponse) + "HK";

    private static String password = "0000000000000000000000000000000000000000000000000000000000000000";


    public static void main(String[] args) throws Exception {

        initializeKeysForTesting();

        ServerConfig exchangeConfig = new ServerConfig("IM EXCHANGE", ServerConfig.Role.Exchange, "localhost", 9000);
        ServerConfig processorConfig = new ServerConfig("IM PROCESSOR", ServerConfig.Role.Processor, "localhost", 9001);

        exchangeConfig.knownConfig = processorConfig;
        processorConfig.knownConfig = exchangeConfig;

        BtcExchange exchange = new BtcExchange(exchangeConfig);
        BtcProcessor processor = new BtcProcessor(processorConfig, fName, 100);

        exchange.connect();

        BtcTransactionRequest req = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 10000, 10000);
        BtcTransactionRequest req2 = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 20000, 30000);

        exchange.addRequest(req);
        exchange.addRequest(req2);

        exchange.sendTransactionRequests();


    }

    private static void initializeKeysForTesting() throws Exception {
        //RSA
        KeyPair kp = RSA.generateKeyPair();
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "PRR", kp.getPrivate().getEncoded());
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "ERP", kp.getPublic().getEncoded());

        kp = RSA.generateKeyPair();
        Properties.setProperty(String.valueOf(MessageType.BtcResponse) + "ERR", kp.getPrivate().getEncoded());
        Properties.setProperty(String.valueOf(MessageType.BtcResponse) + "PRP", kp.getPublic().getEncoded());

        // first AES, infoKey, BtcPrivKey storage
        byte[] exchangePartKey = Sha256.hash("exchangePartKey");
        Properties.setProperty(halfKey32Exchange, exchangePartKey);

        byte[] processorPartKey = Sha256.hash("processorPartKey");
        Properties.setProperty(halfKey32Processor, processorPartKey);

        byte[] AESforInfoKeyEncyption = new byte[64];
        System.arraycopy(exchangePartKey, 0, AESforInfoKeyEncyption, 0, 32);
        System.arraycopy(processorPartKey, 0, AESforInfoKeyEncyption, 32, 32);

        AESforInfoKeyEncyption = Sha256.hash(AESforInfoKeyEncyption);
        byte[] AESforBtcPrivKey = Sha256.hash("AESforBitcoin");

        String btcPrivKey = "0000000000000000000000000000000000000000000000000000000000000000";

        byte[] encryptedPrivKey = AES.encrypt(ByteUtils.hexToBytes(btcPrivKey), AESforBtcPrivKey);


        FileHandler handler = new FileHandler(fName, 100);

        int position = handler.putElementInRandomPlace(encryptedPrivKey);

        byte[] infoKey = new byte[36];
        System.arraycopy(ByteUtils.unsignedToBytes(position), 0, infoKey, 0, 4);
        System.arraycopy(AESforBtcPrivKey, 0, infoKey, 4, 32);

        Properties.setProperty(infoKeyEncrypted, AES.encrypt(infoKey, AESforInfoKeyEncyption));

    }

}
