package pl.edu.pja.s16605;

import pl.edu.pja.s16605.exceptions.InvalidPropertyException;

import java.util.Arrays;
import java.util.prefs.Preferences;

public class Properties {

    static Preferences preferences = Preferences.userNodeForPackage(Properties.class);

    public static byte[] getProperty(String name) throws InvalidPropertyException {
        byte[] property = preferences.getByteArray(name, new byte[0]);
        if (Arrays.equals(property, new byte[0]))
            throw new InvalidPropertyException();
        else return property;
    }

    public static void setProperty(String name, byte[] property) {
        preferences.putByteArray(name, property);
    }


}
