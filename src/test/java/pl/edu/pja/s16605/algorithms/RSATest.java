package pl.edu.pja.s16605.algorithms;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class RSATest {


    static private KeyPair pair;

    @BeforeAll
    static void start() {
        try {
            pair = RSA.generateKeyPair();
        } catch (Exception e) {
            fail();
        }
    }


    @Test
    void generateKeyPair() {
        try {
            RSA.generateKeyPair();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void encryptAndDecrypt() {
        try {
            byte[] encrypted = RSA.encrypt("TestString".getBytes(), pair.getPublic().getEncoded());
            byte[] decrypted = RSA.decrypt(encrypted, pair.getPrivate().getEncoded());
            assertEquals("TestString", new String(decrypted));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}