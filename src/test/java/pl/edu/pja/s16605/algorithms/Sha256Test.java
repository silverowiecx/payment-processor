package pl.edu.pja.s16605.algorithms;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.fail;

class Sha256Test {


    @Test
    void hash() {
        try {
            Sha256.hash(new byte[]{0});
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void testHash() {
        try {
            Sha256.hash("TestString");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            fail();
        }
    }
}