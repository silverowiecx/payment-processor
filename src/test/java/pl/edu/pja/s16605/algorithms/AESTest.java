package pl.edu.pja.s16605.algorithms;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static org.junit.jupiter.api.Assertions.fail;


class AESTest {

    private static byte[] key;

    @BeforeAll
    static void start() {
        key = new byte[32];
        new SecureRandom().nextBytes(key);
    }


    @Test
    void encryptAndDecrypt() {
        byte[] encrypted = new byte[0];
        try {
            encrypted = AES.encrypt("TestString".getBytes(), key);
            byte[] decrypted = AES.decrypt(encrypted, key);
            assert (new String(decrypted).equals("TestString"));
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            fail();
        }

    }

}