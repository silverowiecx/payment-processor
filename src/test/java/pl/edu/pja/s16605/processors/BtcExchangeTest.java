package pl.edu.pja.s16605.processors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.edu.pja.s16605.communication.ServerConfig;
import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class BtcExchangeTest {

    static BtcExchange exchange;

    @BeforeAll
    static void start() throws Exception {
        exchange = new BtcExchange(new ServerConfig("EXCHANGE", ServerConfig.Role.Exchange, "localhost", 1001));

    }

    @Test
    void addRequest() {
        BtcTransactionRequest request = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 20000, 30000);
        try {
            exchange.addRequest(request);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(exchange.requests.get(0), request);
    }

    @Test
    void sendTransactionRequests() {
        // ?
    }

    @Test
    void processResponse() {
        // ?
    }
}