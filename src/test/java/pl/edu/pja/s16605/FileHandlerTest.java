package pl.edu.pja.s16605;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


class FileHandlerTest {

    static FileHandler handler;
    int pos = 0, len = 0;

    @BeforeAll
    static void prepare() throws Exception {
        handler = new FileHandler("Test", 400);
    }


    @Test
    void regenerateFile() throws Exception {
        handler.regenerateFile();
    }

    @Test
    void putElement() throws Exception {
        byte[] testElement = "TestElement".getBytes();
        len = testElement.length;
        handler.putElement(testElement, pos);
    }

    @Test
    void getElement() throws Exception {
        putElement();
        assert (new String(handler.getElement(pos, len))).equals("TestElement");
    }

    @Test
    void putElementInRandomPlace() throws Exception {
        byte[] testElement = "TestElement".getBytes();
        pos = handler.putElementInRandomPlace(testElement);
        assert (new String(handler.getElement(pos, testElement.length))).equals("TestElement");
    }

}