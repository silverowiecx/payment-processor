package pl.edu.pja.s16605.utils;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class ByteUtilsTest {

    private String hex = "0000ffaa";
    private long asInt = 65450;
    private byte[] bytes = {0, 0, -1, -86};


    @Test
    void unsignedToBytes() {
        assert (Arrays.equals(ByteUtils.unsignedToBytes(asInt), bytes));
    }

    @Test
    void bytesToUnsigned() {
        assert (ByteUtils.bytesToUnsigned(bytes) == asInt);
    }

    @Test
    void bytesToHex() {
        assert (ByteUtils.bytesToHex(bytes).equals(hex));
    }

    @Test
    void hexToBytes() {
        assert (Arrays.equals(bytes, ByteUtils.hexToBytes(hex)));
    }

    @Test
    void switchEndianness() {
        assert (ByteUtils.bytesToHex(ByteUtils.switchEndianness(ByteUtils.hexToBytes(hex))).equals("aaff0000"));
    }
}