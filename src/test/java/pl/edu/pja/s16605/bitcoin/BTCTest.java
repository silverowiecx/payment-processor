package pl.edu.pja.s16605.bitcoin;

import org.junit.jupiter.api.Test;
import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;
import pl.edu.pja.s16605.exceptions.BtcExceptions.ElectrumException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class BTCTest {

    @Test
    void getBalance() {
        try {
            BTC.getBalance();
        } catch (ElectrumException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    void payTo() {
        // ??
    }

    @Test
    void payToMany() {
        // ??
    }

    @Test
    void parseRequests() {
        ArrayList<BtcTransactionRequest> requests = new ArrayList<>();
        BtcTransactionRequest req = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 10000, 10000);
        BtcTransactionRequest req2 = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 20000, 30000);
        requests.add(req);
        requests.add(req2);
        String requestsToString = BTC.parseRequests(requests);

        assertEquals("[[\"msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r\", \"0.00010000\"], [\"msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r\", \"0.00020000\"]]", requestsToString);


    }
}