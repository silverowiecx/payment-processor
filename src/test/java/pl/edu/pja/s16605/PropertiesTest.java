package pl.edu.pja.s16605;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.edu.pja.s16605.exceptions.InvalidPropertyException;

class PropertiesTest {


    @BeforeAll
    static void start() {
        Properties.preferences.putByteArray("TEST", "TEST".getBytes());
    }

    @Test
    void getProperty() throws InvalidPropertyException {
        assert (new String(Properties.getProperty("TEST")).equals("TEST"));
    }

    @Test
    void setProperty() throws InvalidPropertyException {
        Properties.setProperty("TEST2", "TEST2".getBytes());
        assert (new String(Properties.getProperty("TEST2")).equals("TEST2"));
    }
}