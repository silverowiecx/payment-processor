package pl.edu.pja.s16605.communication.messages;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.edu.pja.s16605.Properties;
import pl.edu.pja.s16605.algorithms.RSA;
import pl.edu.pja.s16605.algorithms.Sha256;
import pl.edu.pja.s16605.communication.requests.BtcTransactionRequest;
import pl.edu.pja.s16605.utils.Timestamp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.KeyPair;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class BtcRequestTest {

    @BeforeAll
    private static void initializeKeysForTesting() throws Exception {
        //RSA
        KeyPair kp = RSA.generateKeyPair();
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "PRR", kp.getPrivate().getEncoded());
        Properties.setProperty(String.valueOf(MessageType.BtcRequest) + "ERP", kp.getPublic().getEncoded());


    }

    @Test
    void test() {
        try {
            ArrayList<BtcTransactionRequest> requests = new ArrayList<>();
            BtcTransactionRequest req = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 10000, 10000);
            BtcTransactionRequest req2 = new BtcTransactionRequest("msjiTa367tqQqa7baC1y7xgjX4tvGJeK1r", 20000, 30000);
            requests.add(req);
            requests.add(req2);
            BtcRequest request = new BtcRequest(Timestamp.get(), 0, Sha256.hash("TestString"), requests);

            ByteArrayOutputStream testOutputStream = new ByteArrayOutputStream();
            request.serialize(testOutputStream, String.valueOf(MessageType.BtcRequest) + "ERP");

            ByteArrayInputStream testInputStream = new ByteArrayInputStream(testOutputStream.toByteArray());

            byte[] msgType = new byte[1];
            testInputStream.read(msgType);

            assertEquals(msgType[0], MessageType.BtcRequest);

            BtcRequest deserializedRequest = new BtcRequest(testInputStream, String.valueOf(MessageType.BtcRequest) + "PRR");

            assertEquals(request, deserializedRequest);


        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }
}