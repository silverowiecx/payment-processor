package pl.edu.pja.s16605.communication.messages;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.edu.pja.s16605.Properties;
import pl.edu.pja.s16605.algorithms.RSA;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.KeyPair;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class BtcResponseTest {


    @BeforeAll
    static void generateKeysForTesting() throws Exception {
        KeyPair kp = RSA.generateKeyPair();
        kp = RSA.generateKeyPair();
        Properties.setProperty(String.valueOf(MessageType.BtcResponse) + "ERR", kp.getPrivate().getEncoded());
        Properties.setProperty(String.valueOf(MessageType.BtcResponse) + "PRP", kp.getPublic().getEncoded());
    }


    @Test
    void test() {
        try {
            BtcResponse response = new BtcResponse("00aaffcc", 0);
            ByteArrayOutputStream testOutputStream = new ByteArrayOutputStream();

            response.serialize(testOutputStream, String.valueOf(MessageType.BtcResponse) + "PRP");

            ByteArrayInputStream testInputStream = new ByteArrayInputStream(testOutputStream.toByteArray());
            byte[] msgType = new byte[1];

            testInputStream.read(msgType);
            assertEquals(msgType[0], MessageType.BtcResponse);

            BtcResponse deserializedResponse = new BtcResponse(testInputStream, String.valueOf(MessageType.BtcResponse) + "ERR");
            assertEquals(deserializedResponse, response);

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}